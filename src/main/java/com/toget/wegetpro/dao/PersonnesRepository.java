package com.toget.wegetpro.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.toget.wegetpro.entites.Personnes;

public interface PersonnesRepository extends JpaRepository<Personnes, Long>{

}
