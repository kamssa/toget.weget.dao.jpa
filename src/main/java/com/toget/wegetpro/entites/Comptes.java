package com.toget.wegetpro.entites;

import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

@Entity
public class Comptes extends AbstractEntity{
	
	private static final long serialVersionUID = 1L;

	private String login;
	private String password;
	private String email;
	private LocalDate dateCreation; 
	private boolean actived;
	
	@OneToOne(fetch= FetchType.LAZY)
	@JoinColumn(name="id_membres")
	private Membres membres;
	
	@Column(name="id_membres",insertable= false,updatable=false)
	private long idMembres;
	
	@OneToOne(fetch= FetchType.LAZY)
	@JoinColumn(name="id_administrateurs")
	private Administrateurs administrateurs;
	
	@Column(name="id_administrateurs",insertable= false,updatable=false)
	private long idAdministrateurs;
	
	
	public Comptes() {
		super();
		
	}
	
	public Comptes(String login, String password) {
		super();
		this.login = login;
		this.password = password;
	}
	

	public Comptes(String login, String password, String email, boolean actived) {
		super();
		this.login = login;
		this.password = password;
		this.email = email;
		this.actived = actived;
	}

	public String getLogin() {
		return login;
	}
	public void setLogin(String login) {
		this.login = login;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public boolean isActived() {
		return actived;
	}
	public void setActived(boolean actived) {
		this.actived = actived;
	}

	public LocalDate getDateCreation() {
		return dateCreation;
	}

	public void setDateCreation(LocalDate dateCreation) {
		this.dateCreation = dateCreation;
	}

	public long getIdMembres() {
		return idMembres;
	}

	public long getIdAdministrateurs() {
		return idAdministrateurs;
	}

	@Override
	public String toString() {
		return "Comptes [login=" + login + ", password=" + password + ", email=" + email + ", dateCreation="
				+ dateCreation + ", membres=" + membres + "]";
	}
	
	
}
