package com.toget.wegetpro.entites;

import java.io.Serializable;

import javax.persistence.Embeddable;

@Embeddable
public class Adresses implements Serializable {
	
	
	private static final long serialVersionUID = 1L;
	private String codePostal;
	private String quartier;
	private String ville;
	

	private String mobile;
	private String bureau;
	private String fixe;
	public String getCodePostal() {
		return codePostal;
	}
	public void setCodePostal(String codePostal) {
		this.codePostal = codePostal;
	}
	public String getQuartier() {
		return quartier;
	}
	public void setQuartier(String quartier) {
		this.quartier = quartier;
	}
	public String getVille() {
		return ville;
	}
	public void setVille(String ville) {
		this.ville = ville;
	}
	
	public String getMobile() {
		return mobile;
	}
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
	public String getBureau() {
		return bureau;
	}
	public void setBureau(String bureau) {
		this.bureau = bureau;
	}
	public String getFixe() {
		return fixe;
	}
	public void setFixe(String fixe) {
		this.fixe = fixe;
	}
	
}
