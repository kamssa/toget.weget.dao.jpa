package com.toget.wegetpro.entites;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue("ME")
public class Membres extends Personnes {

   private static final long serialVersionUID = 1L;
	
    private String nomSociete;
    private String description;
    private String diplome;
    private String anneExperience;
    
    
    
	public Membres(String titre, String nom, String prenom) {
		super(titre, nom, prenom);
	}
	
	public Membres(String titre, String nom, String prenom, String nomSociete, String description, String diplome,
			String anneExperience) {
		super(titre, nom, prenom);
		this.nomSociete = nomSociete;
		this.description = description;
		this.diplome = diplome;
		this.anneExperience = anneExperience;
	}

	public String getNomSociete() {
		return nomSociete;
	}
	public void setNomSociete(String nomSociete) {
		this.nomSociete = nomSociete;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getDiplome() {
		return diplome;
	}
	public void setDiplome(String diplome) {
		this.diplome = diplome;
	}
	public String getAnneExperience() {
		return anneExperience;
	}
	public void setAnneExperience(String anneExperience) {
		this.anneExperience = anneExperience;
	}

	@Override
	public String toString() {
		return "Membres [nomSociete=" + nomSociete + ", description=" + description + ", diplome=" + diplome
				+ ", anneExperience=" + anneExperience + "]";
	}
    
    
}
