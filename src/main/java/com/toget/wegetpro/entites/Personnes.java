package com.toget.wegetpro.entites;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;







@Entity
@Inheritance(strategy=InheritanceType.JOINED)
@DiscriminatorColumn(name="TYPE_PERS",discriminatorType=DiscriminatorType.STRING,length=2)
@jsonTypeInfo(use= JsonTypeInfo.Id.NAME,include= JsonTypeInfo.As.PROPERTY, property="type")
@JsonSubType({
@type(name="ME",value=Membres.class),
@type(name="AD",value=Administrateurs.class)	
})
public abstract class Personnes extends AbstractEntity{
	
    private static final long serialVersionUID = 1L;
	private String titre;
    private String nom;
	private String prenom;
	private String cni;
	private String nomComplet;
    private String pathPhoto;
    
    @Embedded
    private Adresses adresse;
    
    @OneToMany(fetch= FetchType.EAGER, cascade= CascadeType.ALL)
    @JoinColumn(name="id_telephones")
	private List<Telephones> telephones;
    
    public Personnes(String titre, String nom, String prenom) {
		super();
		this.titre = titre;
		this.nom = nom;
		this.prenom = prenom;
	}

	public String getTitre() {
		return titre;
	}
	
	public void setTitre(String titre) {
		this.titre = titre;
	}
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public String getPrenom() {
		return prenom;
	}
	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}
	public String getCni() {
		return cni;
	}
	public void setCni(String cni) {
		this.cni = cni;
	}
	public String getNomComplet() {
		return nomComplet;
	}
	
	@PrePersist
	@PreUpdate
	public void setNomComplet() {
		this.nomComplet = nom + " " + prenom;
	}
	public String getPathPhoto() {
		return pathPhoto;
	}
	public void setPathPhoto(String pathPhoto) {
		this.pathPhoto = pathPhoto;
	}
	@Override
	public String toString() {
		return String.format("Personne[%s,%s,%s,%s]", id, titre, nomComplet, version);
	}
    }
