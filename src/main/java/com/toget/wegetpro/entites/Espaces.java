package com.toget.wegetpro.entites;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class Espaces extends AbstractEntity {

	private static final long serialVersionUID = 1L;
    
	private double prix;
    
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="id_membres")
	private Membres membres;
    @Column(name="id_membres",insertable=false, updatable= false)
    private long idMembres;
    
	public Espaces(double prix, Membres membres) {
		super();
		this.prix = prix;
		this.membres = membres;
	}

	public double getPrix() {
		return prix;
	}

	public void setPrix(double prix) {
		this.prix = prix;
	}

	@Override
	public String toString() {
		return "Espaces [prix=" + prix + ", membres=" + membres + "]";
	}
    
    
}
