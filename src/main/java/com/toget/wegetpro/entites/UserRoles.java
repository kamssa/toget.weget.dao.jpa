package com.toget.wegetpro.entites;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class UserRoles extends AbstractEntity {

	private static final long serialVersionUID = 1L;

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name = "id_personne")
	private Personnes personnes;
	@Column(name = "id_personne", insertable = false, updatable = false)
	private long idPersonne;

	@ManyToOne(fetch= FetchType.LAZY)
	@JoinColumn(name = "id_roles")
	private AppRoles roles;
	@Column(name = "id_Roles", insertable = false, updatable = false)
	private long idRoles;

	public Personnes getPersonnes() {
		return personnes;
	}

	public void setPersonnes(Personnes personnes) {
		this.personnes = personnes;
	}

	public long getIdPersonne() {
		return idPersonne;
	}

	public AppRoles getRoles() {
		return roles;
	}

	public void setRoles(AppRoles roles) {
		this.roles = roles;
	}

	public long getIdRoles() {
		return idRoles;
	}

}
