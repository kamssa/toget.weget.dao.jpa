package com.toget.wegetpro.entites;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue("AD")
public class Administrateurs extends Personnes{

	
	private static final long serialVersionUID = 1L;

	public Administrateurs(String titre, String nom, String prenom) {
		super(titre, nom, prenom);
		
	}

	@Override
	public String toString() {
		return "Administrateurs [getTitre()=" + getTitre() + ", getNom()=" + getNom() + ", getPrenom()=" + getPrenom()
				+ "]";
	}

}
